// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "../TPS.h"
#include "../StateEffect/Interface/TPSIGameActor.h"


void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTPStateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UTPStateEffect* myEffect = Cast<UTPStateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
				
					if (!myEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UTPStateEffect*> CurrentEffects;
						ITPSIGameActor* myInterface = Cast<ITPSIGameActor>(TakeEffectActor);
						
						if(myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();

						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
						

					}
					else
					{
						bIsCanAddEffect = true;

					}

					if (bIsCanAddEffect)
					{
						bIsHavePossibleSurface = true;
						UTPStateEffect* NewEffect = NewObject<UTPStateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor);
						}

					}

				}
				i++;
			}
		}
	}
}
