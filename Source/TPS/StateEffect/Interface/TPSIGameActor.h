// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../TPStateEffect.h"
#include "TPS/Type/Types.h"
#include "TPSIGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTPSIGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_API ITPSIGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTPStateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTPStateEffect* RemoveEffect);
	virtual void AddEffect(UTPStateEffect* newEffect);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);

};
