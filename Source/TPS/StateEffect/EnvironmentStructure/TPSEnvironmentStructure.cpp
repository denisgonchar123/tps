// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSEnvironmentStructure.h"
#include "Materials/MaterialInstance.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ATPSEnvironmentStructure::ATPSEnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATPSEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPSEnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPSEnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));

	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;

		}
	}

	return Result;
}

TArray<UTPStateEffect*> ATPSEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATPSEnvironmentStructure::RemoveEffect(UTPStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATPSEnvironmentStructure::AddEffect(UTPStateEffect* newEffect)
{
	Effects.Add(newEffect);
}

