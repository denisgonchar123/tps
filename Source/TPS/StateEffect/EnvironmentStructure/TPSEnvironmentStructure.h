// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Interface/TPSIGameActor.h"
#include "../TPStateEffect.h"
#include "TPSEnvironmentStructure.generated.h"

UCLASS()
class TPS_API ATPSEnvironmentStructure : public AActor, public ITPSIGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPSEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	TArray<UTPStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTPStateEffect* RemoveEffect) override;
	void AddEffect(UTPStateEffect* newEffect) override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UTPStateEffect*> Effects;

};
