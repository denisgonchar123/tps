// Fill out your copyright notice in the Description page of Project Settings.


#include "TPStateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "Interface/TPSIGameActor.h"
#include "../Character/Healths/TPSHeaithComponent.h"

bool UTPStateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	ITPSIGameActor* myInterface = Cast<ITPSIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UTPStateEffect::DestroyObject()
{
	ITPSIGameActor* myInterface = Cast<ITPSIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
	
}

bool UTPStateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	
	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);
		
		GetWorld()->GetTimerManager().SetTimer(TimerHandel_EffectTimer, this, &UTPStateEffect_ExecuteOnce::DestroyObject, Timer, false);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);

	}
	ExecuteOnce();
	
	return true;
}

void UTPStateEffect_ExecuteOnce::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTPStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPSHeaithComponent* myHealthComp = Cast<UTPSHeaithComponent>(myActor->GetComponentByClass(UTPSHeaithComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);

		}

	}

	//DestroyObject();

}

bool UTPStateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandel_EffectTimer, this, &UTPStateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandel_ExecuteTimer, this, &UTPStateEffect_ExecuteTimer::Execute, RateTimer, true);
	
	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);
		
		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		
	}

	 return true;
}

void UTPStateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
	
}

void UTPStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTPSHeaithComponent* myHealthComp = Cast<UTPSHeaithComponent>(myActor->GetComponentByClass(UTPSHeaithComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

}

bool UTPStateEffect_ExecuteHealthTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandel_EffectTimer, this, &UTPStateEffect_ExecuteHealthTimer::DestroyObject, Timer, false);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);
		
		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}

	Execute();

	return true;
}

void UTPStateEffect_ExecuteHealthTimer::DestroyObject()
{
	if (myActor)
	{
		FuncCastHealth(myActor);
		myHealthComponent->bIsTakingDamage = true;

	}
	
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTPStateEffect_ExecuteHealthTimer::Execute()
{
	FuncCastHealth(myActor);

	if (myHealthComponent)
	{
		myHealthComponent->bIsTakingDamage = false;
	}

}

void UTPStateEffect_ExecuteHealthTimer::FuncCastHealth(AActor* NewActor)
{	
	if (NewActor)
	{
		if (!myHealthComponent)
		{
			UTPSHeaithComponent* myHealthComp = Cast<UTPSHeaithComponent>(myActor->GetComponentByClass(UTPSHeaithComponent::StaticClass()));

			myHealthComponent = myHealthComp;
		}
	}
	
	
}
