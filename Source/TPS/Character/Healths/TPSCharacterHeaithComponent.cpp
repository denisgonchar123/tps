// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSCharacterHeaithComponent.h"

void UTPSCharacterHeaithComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{		
		ChangeShieldValue(ChangeValue);
	
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CollDownShieldTimer);

		if (DeadShield && Shield < 0.0f)
		{
			UE_LOG(LogTemp, Error, TEXT("Sheld < 0 "));
			OnShieldEffect.Broadcast();
			DeadShield = false;
		}

		Super::ChangeHealthValue(ChangeValue);
		
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTPSCharacterHeaithComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
		}
	}
	
}

float UTPSCharacterHeaithComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHeaithComponent::ChangeShieldValue(float ChangeValue)
{	
	DeadShield = true;

	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CollDownShieldTimer);
	
	Shield = FMath::Clamp(Shield + ChangeValue, 0.0f, ShieldMax);
	
	OnChieldChange.Broadcast(Shield, ChangeValue);
	
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTPSCharacterHeaithComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	/*
	Shield += ChangeValue;
	
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}
	
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTPSCharacterHeaithComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
		
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}*/
	
	OnChieldChange.Broadcast(Shield, ChangeValue);

	UE_LOG(LogTemp, Error, TEXT("UTPSCharacterHeaithComponent::ChangeShieldValue - Sheld > 0 "));
	OnShieldEffect.Broadcast();
}

void UTPSCharacterHeaithComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTPSCharacterHeaithComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTPSCharacterHeaithComponent::RecoveryShield()
{
	Shield = FMath::Min(Shield + ShieldRecoveryValue, ShieldMax);

	if (Shield == ShieldMax && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	
	OnChieldChange.Broadcast(Shield, ShieldRecoveryValue);

	/*
	float tmp = Shield;
	tmp += ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield =  100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnChieldChange.Broadcast(Shield, ShieldRecoveryValue);
	
	*/
}
