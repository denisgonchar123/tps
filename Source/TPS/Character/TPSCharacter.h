// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Type/Types.h"
#include "../Weapon/WeaponDefault.h"
#include "Components/WidgetComponent.h"
#include "../StateEffect/Interface/TPSIGameActor.h"
#include "../StateEffect/TPStateEffect.h"
#include "InventoryComponent/TPSInventoryComponent.h"
#include "Healths/TPSCharacterHeaithComponent.h"
#include "TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPSIGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	

public:
	ATPSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UTPSCharacterHeaithComponent* CharHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;


public:

	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRun = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool Walk = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool Aim = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;
	
	//Stamina
	UPROPERTY(BlueprintReadWrite, Category = "Movement | Stamina");
	float CurrentStamina = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement | Stamina", meta = (ClampMin = 0.0f, UIMin = 0.0f))
	float MaxStamina = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement | Stamina", meta = (ClampMin = 0.0f, UIMin = 0.0f))
	float StaminaRestoveVelosity = 0.0f; //�������� �������������� ������������ 
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement | Stamina", meta = (ClampMin = 0.0f, UIMin = 0.0f))
	float SprintStaminaConsumptionVelocity = 0.0f; // �������� ����������� ������������ ������
	UPROPERTY(BlueprintReadOnly)
	bool bIsOutOfStamina = false;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	TArray<UAnimMontage*>DeadsAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UTPStateEffect> AbilityEffect;
	
	//Weapon
	AWeaponDefault* CurrentWeapon = nullptr;

	//For Demo
	UPROPERTY(editAnywhere, BlueprintReadWrite, category = "Demo")
		FName InitWeaponName;

	UDecalComponent* CurrentCursor = nullptr;

	//Effect
	TArray<UTPStateEffect*> Effects;
	

	//Inputs
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	void DropCurrentWeapon();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}
	
	bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);

	//Tick Func
	UFUNCTION()
	void MovementTick(float DeltaTime);
	void UpdateStamina(float DeltaTime);



	//Func
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	
	

	//Stamina 
	void SetIsOutOfStamina(bool bIsOutOfStamina_In);

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
		void RemoveCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	//Interface Func
	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();
	
	//Ability Func
	void TryAbilityEnable();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;
	
	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTPStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTPStateEffect* RemoveEffect) override;
	void AddEffect(UTPStateEffect* newEffect) override;
	//End Interface

	//Character dead
	UFUNCTION(BlueprintCallable)
	void CharDead();
	
	void EnableRandoll();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

};



